# README #

analysis framework for the DAQ output at the PSI practical course 2014

### Software Requirements ###

ROOT >= 5.34

## How to convert the binary files##

The first thing you have to do is to compile the analysis software via make

```
$ make
```

Now you have to specify the folder where the binary data files are stored.
This has to be done in a text file called `datapath.txt` in the same folder
where you run this piece of software, e.g. a text file with

```
/path/to/your/data/
```
Up to now the / at the very end of the path is crucial!

After successfully compiling the software and setting the `datapath.txt` you can
convert the binary files to ROOT format.
There are basically three different options to do this:

### Convert single file ###

To convert a single file you just have to call
```
$ ./daqread RUNNUMBER
```
where `RUNNUMBER` is the number of the run.

### Convert range of single files ###

To convert a single file you have to call
```
$ ./daqread RUNSTART RUNSTOP
```
where `RUNSTART` and `RUNSTOP` are the start and end point of the binary file
range.

### Combine a range of single files into one big statistics file ###

For this you can call the `daqread` with the `-c` flag:
```
$ ./daqread -c RUNSTART RUNSTOP
```

The output ROOT file will be of the form `run_RUNSTART-RUNSTOP.root`.

The program will put the output ROOT files into a folder called `ROOT`
inside your data directory.


## How to analyse the ROOT files ##

The complete analysis is contained in a TTree within `run_RUNNUMBER_ana.root`.
The file creation can be looped over an interval of runs.

### Analyse a single file ###

To analyse a single ROOT file you can call
```
$ ./daqana SAMPLESIZE RUNNUMBER
```
where `SAMPLESIZE` is the number of samples per event.
Here `RUNNUMBER` can also be of the form `RUNSTART-RUNSTOP` to analyse a single
file like `run_RUNSTART-RUNSTOP.root`.

### Analyse a range of single files

For this you can call
```
$ ./daqana SAMPLESIZE RUNSTART RUNSTOP
```
This will create a single analysis ROOT file for each input file.


The tree that is created is called ana and can be viewed by loading the file into
ROOT, e.g. via
```
$ root run_RUNNUMBER_ana.root
```

Now you have access to the whole ana tree and can draw every interesting
histogram, e.g.

```
#!root

ana->Draw("integral[CHANNELNUMBER]")
```
or

```
$ root 'quality_check.cxx("RUNNUMBER")'
```
