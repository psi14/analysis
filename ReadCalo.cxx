void ReadCalo(){
    TFile *f = new TFile("../Data/run_9030_ana.root");
    TTree *t = (TTree*)f->Get("ana");
    
    Double_t max[14];
    t->SetBranchAddress("max",&max);
    Double_t baseline[14];
    t->SetBranchAddress("baseline",&baseline);
    Double_t falling_edge[14];
    t->SetBranchAddress("falling_edge",&falling_edge);

    
    // - - - - - - - - - - - - - -
    Double_t   calo[7], calsum, calsumcorr;
    Double_t scaling[7]={1,1.696,0.896,1.696,1.13,0.979,1.067};
    
    
    // Histograms
    TH1F *h_sum=new TH1F("CaloSum","CaloSum;Energy;Counts",500,-100,15000);
    TH1F *h_sum_corr=new TH1F("CaloSumCorr","CaloSumCorr;Energy;Counts",500,-100,15000);
    TH1F *h_time = new TH1F("Pi Stop","PiStop",100,0,1000);
    TH1F *h_time_cut = new TH1F("Pi Stop Cut","Pi Stop Cut;Counts;Time",100,0,1000);
    TH2F *h_time_ene = new TH2F("Time vs EnergyTotal","Time vs EnergyTotal;Time;Energy",100,0,1000,500,-100,15000);
    TH1F *h_sum_corr_early =new TH1F("CaloSumCorr Early","CaloSumCorr Early;Energy;Counts",500,-100,15000);
    TH1F *h_early =new TH1F("Calo 1","Calo 1;Energy;Counts",500,0,2000);
    h_sum_corr->SetLineColor(2);
    
    TH1F *h[7];
    TH1F *h_corr[7];
    for (int ical=0; ical<7;ical++ ){
        char tit[80];
        sprintf(tit,"calomax_%d-baseline_%d",ical+1,ical+1);
        h[ical]= new TH1F(tit,tit,500,0,2000);
    }

    
    // Loop on entries of the tree
    for (int i=0; i<t->GetEntries();i++) {
        t->GetEntry(i);
        
        calo[0]=max[3]-baseline[3];
        for (int ical=1;ical<7;ical++) calo[ical]=max[7+ical]-baseline[7+ical];
        
        calsum=0;
        calsumcorr=0;
        for (int ical=0;ical<7;ical++){
            h[ical]->Fill(calo[ical]);
            //h_corr[ical]->Fill((int)(calo[ical]*scaling[ical]));
            calsum+=calo[ical];
            calsumcorr+=calo[ical]*scaling[ical];
        }
        h_sum->Fill(calsum);
        h_sum_corr->Fill(calsumcorr);
        //Timing
        h_time->Fill((256-falling_edge[2])*10);
        if(calsumcorr>6840) h_time_cut->Fill((256-falling_edge[2])*10);
        h_time_ene->Fill((256-falling_edge[2])*10,calsumcorr);
        if(((256-falling_edge[2])*10>145) &&((256-falling_edge[2])*10<240)){
            h_sum_corr_early->Fill(calsumcorr);
            h_early->Fill(calo[0]);
        }
    }
    
    TCanvas *c = new TCanvas("c","c",1000,700);
    c->Divide(4,2);
    
    for (int ical=0;ical<7;ical++) {
        c->cd(ical+1);
        h[ical]->Draw();
    }
    c->cd(8)->SetLogy(); h_sum->Draw();
    h_sum_corr->Draw("same");
    
    //Timing

    TCanvas* c2=new TCanvas("c2","c2",1000,700);
    c2->Divide(2,2);
    c2->cd(1);
    h_time->Draw();
    h_time->GetXaxis()->SetTitle("time [ns]");
    h_time->GetYaxis()->SetTitle("counts");
    
    c2->cd(2);
    h_time_cut->Draw();
    
    c2->cd(3);
    h_time_ene->Draw("box");
    c2->cd(4);
    h_sum_corr->Draw();
    
    TCanvas* c3=new TCanvas("c3","c3",1000,700);
    c3->Divide(2,1);
    c3->cd(1);
    h_sum_corr_early->Draw();
    c3->cd(2);
    h_early->Draw();
    
    
    
    
 
 }
