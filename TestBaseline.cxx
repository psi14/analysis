void TestBaseline(){
    //Read in File get Tree's
    TFile *f = new TFile("../Data/ROOT/run_3103-3107_ana.root");
    TTree *t = (TTree*)f->Get("ana");
    Double_t baseline[14];
    t->SetBranchAddress("baseline",&baseline);
    Double_t baseline2[14];
    t->SetBranchAddress("baseline2",&baseline2);
 
    TH1F * h_difference = new TH1F("BL2-BL1","BL2-BL1;counts;difference in Energy",100,-1000,1000);
    
    for(int i=0;i<t->GetEntries();i++){
        t->GetEntry(i);
        cout<<baseline[3]-baseline2[3]<<"\n";
        h_difference->Fill(baseline[3]-baseline2[3]);
    }
    
    h_difference->Draw();
}

