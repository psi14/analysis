DATAPATH=$(cat datapath.txt)
cd $DATAPATH

if [ ! -d "filtered" ]; then
    mkdir filtered    
fi

cd filtered

for FILE in ../*.bin
do
    echo $FILE
    FSIZE=$(stat -c %s "$FILE")
    if [ "$FSIZE" = "57363320" ]; then
        ln -s $FILE
    fi
done

cd -
