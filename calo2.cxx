void calo2(){

TFile *f = new TFile("run_1171_s1024_ana.root","READ");
TTree *t = f->Get("ana");

TCanvas *c2 = new TCanvas("c2","Calo2",2000,1500);
c2->Divide(7,3);

// int max_limitUp[7]={2000,9000,8000,2000,2000,2000,2000};
// int integral_limitUp[7]		;
// int integral_limitLow[7]	;


  for(int i=8; i<=13; i++){
    
    c2->cd(i-6);

    t->Draw(Form("max[%i]-baseline[%i]>>h_max_%i(100,-200,1800)",i,i,i));
    
    TH1F *h_max = (TH1F*)gDirectory->Get(Form("h_max_%i",i));

    h_max->GetXaxis()->SetTitle("max");
    h_max->GetYaxis()->SetTitle("counts");
    
    
    h_max->Draw();
    
    c2->cd(i+1);
    
    

    t->Draw(Form("integral[%i]>>h_integral_%i(200,-200000,200000)",i,i));
    
    TH1F *h_integral = (TH1F*)gDirectory->Get(Form("h_integral_%i",i));

    h_integral->GetXaxis()->SetTitle("integral");
    h_integral->GetYaxis()->SetTitle("counts");
    
    h_integral->Draw();
    
    
    c2->cd(i+8);

    t->Draw(Form("max[%i]-baseline[%i]:integral[%i]>>h_scatter_%i(200,-200000,200000,100,-200,1800)",i,i,i,i));
    TH1F *h_scatter = gDirectory->Get(Form("h_scatter_%i",i));

    h_scatter->GetXaxis()->SetTitle("integral");
    h_scatter->GetYaxis()->SetTitle("max");

    // TH1F *h_scatter = gDirectory->Get("h_scatter");
    // TH1F *h_scatter = (TH1F*)gDirectory->Get("h_scatter");
    h_scatter->Draw();

    c2->cd(i+1);
    t->Draw(Form("integral[%i]>>h_integral_red_%i(200,-200000,200000)",i,i),Form("max[%i]>8400",i),"SAME");
    TH1F *h_integral_red = (TH1F*)gDirectory->Get(Form("h_integral_red_%i",i));
    h_integral_red->SetLineColor(kRed);
    
    h_integral_red->GetXaxis()->SetTitle("integral");
    h_integral_red->GetYaxis()->SetTitle("counts");
    h_integral_red->Draw("SAME");

  }
  
  int i = 3;
  
  c2->cd(1);

    t->Draw(Form("max[%i]-baseline[%i]>>h_max_%i(100,-200,1800)",i,i,i));
    
    TH1F *h_max = (TH1F*)gDirectory->Get(Form("h_max_%i",i));

    h_max->GetXaxis()->SetTitle("max");
    h_max->GetYaxis()->SetTitle("counts");
    
    h_max->Draw();
    
    c2->cd(8);

    t->Draw(Form("integral[%i]>>h_integral_%i(200,-200000,200000)",i,i));
    
    TH1F *h_integral = (TH1F*)gDirectory->Get(Form("h_integral_%i",i));

    h_integral->GetXaxis()->SetTitle("integral");
    h_integral->GetYaxis()->SetTitle("counts");
    
    h_integral->Draw();
    
    c2->cd(15);

    t->Draw(Form("max[%i]-baseline[%i]:integral[%i]>>h_scatter_%i(200,-200000,200000,100,-200,1800)",i,i,i,i));
    TH1F *h_scatter = (TH1F*)gDirectory->Get(Form("h_scatter_%i",i));

    h_scatter->GetXaxis()->SetTitle("integral");
    h_scatter->GetYaxis()->SetTitle("max");

    // TH1F *h_scatter = gDirectory->Get("h_scatter");
    // TH1F *h_scatter = (TH1F*)gDirectory->Get("h_scatter");
    h_scatter->Draw();

    c2->cd(8);
    t->Draw(Form("integral[%i]>>h_integral_red_%i(200,-200000,200000)",i,i),Form("max[%i]>8400",i),"SAME");
    TH1F *h_integral_red = (TH1F*)gDirectory->Get(Form("h_integral_red_%i",i));
    h_integral_red->SetLineColor(kRed);
    
    h_integral_red->GetXaxis()->SetTitle("integral");
    h_integral_red->GetYaxis()->SetTitle("counts");
    h_integral_red->Draw("SAME");

}