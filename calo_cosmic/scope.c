#include "iostream.h"
void scope(){
  char fcalo1[80] = "F1cosmics_pmt07_00000.txt";
  char ftime[80] = "F2cosmics_pmt07_00000.txt"; 
  
  Double_t ene, n_ene, time, n_time; 

  TH1F *hene = new TH1F("hcalo1","PMT1 scope; nVs",250,-50,200); 
  TH1F *htime = new TH1F("htime","t_pistop - t_trig scope; ns",500,-500,1500); 
  
  ifstream fin1; 
  fin1.open(fcalo1); 
  while (fin1) { 
    fin1 >> ene >> n_ene;
    hene->Fill(ene*-1000000000.,n_ene); 
  }
  fin1.close(); 

  ifstream fin2; 
  fin2.open(ftime); 
  while (fin2) { 
    fin2 >> time >> n_time;
    htime->Fill(time*-1000000000.,n_time); 
  }
  fin2.close(); 

  TCanvas *c = new TCanvas("scope","scope",700,700); 
  c->Divide(1,2); 
  
  c->cd(1); 
  hene->Draw(); 
  c->cd(2); 
  htime->Rebin(2);
  htime->Draw(); 
  
  TF1 *ffit = new TF1("tfit","[0]*(1-exp(-(x-[1])/[2]))*exp(-(x-[1])/[3])+[4]",-200,1000);
  ffit->SetParNames("norm","offset","taupi","taumu","bkg");
  ffit->SetParameters(35,50,26,2200,70);
  ffit->SetParLimits(2,10,100);
  //ffit->SetParLimits(3,1000,5000);
  //ffit->FixParameter(2,26);
  ffit->FixParameter(3,2200);
  //ffit->Draw("same");
  htime->Fit(ffit,"","",0,900);
}
