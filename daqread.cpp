/**
 * PSI14 - daqread.C
 *
 * read binary files from the output of the two logic box DAQ
 * and write them to a corresponding ROOT file in a TTree
 *
 * @author Carsten Grzesik <grzesik@physi.uni-heidelberg.de>
 * @author Sebastian Schenk <Sebastian.Schenk@stud.uni-heidelberg.de>
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <TFile.h>
#include <TTree.h>

#include "helper_functions.hpp"

// get the size of a file in bytes
long getfilesize(FILE *file)
{
    long curpos, endpos;
    curpos = ftell(file);
    fseek(file, 0, 2);
    endpos = ftell(file);
    fseek(file, curpos, 0);
    
    return endpos;
}

int daqread(uint32_t runnumber)
{
    /**
     * read the header file of our binaries given in the consecutive form
     * run number           32 bit
     * UTC timestamp        64 bit
     * event sample size     8 bit
     * clock                 8 bit
     * free spacing         16 bit
     * 
     * TOTAL size of header: 16 bytes
     */
    FILE *filep;
    FILE *pathp;
    
    char fpath[256];
    char fname[256];
    pathp = fopen("datapath.txt", "r");
    if (!pathp) {
        printf("Unable to read path to data file \n");
        return -1;
    }
    fgets(fpath, sizeof(fpath), pathp);
    sprintf(fname, "%srun_%u.bin", strtok(fpath, "\n"), runnumber);
    fclose(pathp);
    
    // open file
    filep = fopen(fname, "rb");
    if (!filep) {
        printf("Unable to open file %s \n", fname);
        return -1;
    }
    
    // get filesize in bytes
    long size = getfilesize(filep);
    
    // read header
    uint32_t buffer[6];
    if (!fread(buffer, sizeof(uint32_t), 4, filep)) {
        printf("Unable to read file %s \n", fname);
        return -1;
    } else {
        printf("Reading %s \n", fname);
    }
    
    uint32_t run = buffer[0];
    uint64_t UTC_timestamp = *((uint64_t *)(&(buffer[1])));
    uint8_t eventsamplesize = buffer[3];
    uint8_t clock = buffer[3] >> 8;
    
    /** change encoded numbers to useful information
     *
     * encoding clock:
     * 0      100 MHz
     * 1       50 MHz
     * 2       33 MHz
     * 3       25 MHz
     *
     * encoding event sample size:
     * 0           16
     * 1           32
     * 2           64
     * 3          128
     * 4          256
     * 5          512
     * 6         1024
     * 7         2048
     */
    uint16_t heventsamplesize = pow(2, 4 + eventsamplesize);
    uint16_t hclock;
    switch (clock) {
        case 0:
            hclock = 100;
            break;
        case 1:
            hclock = 50;
            break;
        case 2:
            hclock = 33;
            break;
        case 3:
            hclock = 25;
            break;
    }
    printf("size: %li bytes \n", size);
    printf("run number: %u \n", run);
    printf("UTC timestamp: %lu s \n", UTC_timestamp);
    printf("event sample size: %u \n", heventsamplesize);
    printf("clock: %u MHz \n", hclock);
    
    
    
    /////////////////////// create ROOT file
    // the binary file is always in a form like
    // event number 1, timestamp 1
    // event number 2, timestamp 2
    
    // create subfolder in data directory for root files
    struct stat st = {0};
    char rootpath[256];
    sprintf(rootpath, "%s/ROOT", strtok(fpath, "\n"));
    if (stat(rootpath, &st) == -1) {
        mkdir(rootpath, 0700);
    }
    
    
    char rootfname[256];
    sprintf(rootfname, "%sROOT/run_%u.root", strtok(fpath, "\n"), run);
    TFile * rootfile = new TFile(rootfname, "RECREATE");
    
    TTree * tree = new TTree("raw","raw");
    
    const unsigned int NCHANNELS = 14;
    
    char branchname[256];
    char branchtype[256];
    
    uint16_t channeldata[NCHANNELS][heventsamplesize];
    uint32_t ts1, ts2;
    uint32_t eventnr1, eventnr2;
    uint32_t channels, samples;
    
    tree->Branch("ts1", &ts1);
    tree->Branch("ts2", &ts2);
    tree->Branch("run", &run);
     
    for(unsigned int i=0; i < NCHANNELS; i++){
        sprintf(branchname, "c%02i", i);
        sprintf(branchtype, "channel[%i]/s", heventsamplesize);
        tree->Branch(branchname, channeldata[i], branchtype);
    }
    
    uint32_t mismatchedevents = 0;
    
    while(!feof(filep)){
        fread(buffer, sizeof(uint32_t), 6, filep);       
        // set ts, eventnr, channels, samples
        eventnr1 = buffer[0];
        ts1 = buffer[1];
        eventnr2 = buffer[2];
        ts2 = buffer[3];
        
        fread(&(channeldata[0][0]), sizeof(uint16_t), 
                NCHANNELS*heventsamplesize, filep);
	
	    // check consistency
	    // *WARNING* events with mismatching eventnr are ignored
        if ( abs(eventnr1 - eventnr2) > 5 ) {
		    mismatchedevents++;
		    continue;
	    }
                
        tree->Fill();
    } 
 
    
    // close files
    fclose(filep);
    printf("Number of ignored events (mismatching eventnumber): %u \n", mismatchedevents);
    
    tree->Write();
    rootfile->Close();
    
    printf("Written %s to %s \n", fname, rootfname);
    
    return 0; 
}

int daqread_interval(uint32_t runstart, uint32_t runstop)
{
    if (runstart > runstop) {
	    uint32_t tmp = runstop;
	    runstop = runstart;
	    runstart = tmp;
    }
    
    for (unsigned int run = runstart; run <= runstop; run++) {
	    daqread(run);
    }
    
    return 0;
}

int daqread_interval_combine(uint32_t runstart, uint32_t runstop)
{
    if (runstart > runstop) {
        uint32_t tmp = runstop;
        runstop = runstart;
        runstart = tmp;
    }
    // see daqread for header description
    FILE *filep;
    FILE *pathp;
    
    char fpath[256];
    char fname[256];
    char rootpath[256];
    pathp = fopen("datapath.txt", "r");
    if (!pathp) {
        printf("Unable to read path to data file \n");
        return -1;
    }
    // create subfolder in data directory for root files
    fgets(fpath, sizeof(fpath), pathp);
    struct stat st = {0};
    sprintf(rootpath, "%s/ROOT", strtok(fpath, "\n"));
    if (stat(rootpath, &st) == -1) {
        mkdir(rootpath, 0700);
    }
    fgets(fpath, sizeof(fpath), pathp);
 
    uint32_t buffer[6];
    
    char rootfname[256];
    sprintf(rootfname, "%sROOT/run_%u-%u.root", strtok(fpath, "\n"), runstart, runstop);
    TFile * rootfile = new TFile(rootfname, "RECREATE");    
    TTree * tree = new TTree("raw","raw");
        
    const unsigned int NCHANNELS = 14;
        
    char branchname[256];
    char branchtype[256];
    uint32_t runnum;
    uint32_t ts1, ts2;
    uint32_t eventnr1, eventnr2;
    uint32_t channels, samples;
        
    tree->Branch("ts1", &ts1);
    tree->Branch("ts2", &ts2);
    tree->Branch("run", &runnum);
    
    uint16_t channeldata[NCHANNELS][1024];
        
         
    for(unsigned int i=0; i < NCHANNELS; i++){
        sprintf(branchname, "c%02i", i);
        // WARNING hardcoding!
        sprintf(branchtype, "channel[%i]/s", 1024);//heventsamplesize);
        tree->Branch(branchname, channeldata[i], branchtype);
    }
    
    uint32_t totalmismatch = 0;
    uint32_t notread = 0;
    uint32_t read = 0;
    for (uint16_t run = runstart; run <= runstop; run++) {
        sprintf(fname, "%srun_%u.bin", strtok(fpath, "\n"), run);
    
        // open file
        filep = fopen(fname, "rb");
        if (!filep) {
            printf("Unable to open file %s - skipping \n", fname);
            continue;
        }
        
        // read header
        if (!fread(buffer, sizeof(uint32_t), 4, filep)) {
            printf("Unable to read file %s - skipping \n", fname);
            continue;
        } else {
            printf("Reading %s \n", fname);
            ++ read;
        }
        
        runnum = buffer[0];
        // see daqread() for decoding
        uint8_t eventsamplesize = buffer[3];
        uint16_t heventsamplesize = pow(2, 4 + eventsamplesize);

        /////////////////////// create ROOT file
        // the binary file is always in a form like
        // event number 1, timestamp 1
        // event number 2, timestamp 2
	
        
	    uint32_t mismatchedevents = 0;
        uint32_t breakpoint = 0;
	
        while(!feof(filep)){
            if (!fread(buffer, sizeof(uint32_t), 6, filep)) {
                ++notread;
                break;
            }
           
            
            // set ts, eventnr, channels, samples
            eventnr1 = buffer[0];
            ts1 = buffer[1];
            eventnr2 = buffer[2];
            ts2 = buffer[3];
	    
	        if (!fread(&(channeldata[0][0]), sizeof(uint16_t), 
                    NCHANNELS*heventsamplesize, filep)) {
                ++notread;
                break;
            }
                    
            // check consistency
	        // remove mismatching events by difference between eventnr1 and 
            // eventnr2
	        // *WARNING* events with mismatching eventnr are ignored
            if ( abs(eventnr1 - eventnr2) > 5 ) {
		        mismatchedevents++;
		        continue;
	        }  
	       
            tree->Fill();
        }
        // close files
        fclose(filep);
	    printf("Number of ignored events (mismatching eventnumber): %u \n", mismatchedevents);
	    totalmismatch += mismatchedevents;
    }
    printf("Total number of ignored events (mismatch): %u \n", totalmismatch);
    printf("Total number of ignored events (not readable): %u \n", notread);
    printf("Total number of files read: %u \n", read);
    printf("Total number of events: %u \n", tree->GetEntries());
    tree->Write();
    rootfile->Close();
    fclose(pathp);
    printf("Written to %s \n", rootfname);
    
    return 0;
}

int main(int argc, char **argv)
{
    if ((argc == 4) && (strcmp(argv[1],"-c") == 0)) {
        uint32_t runstart = strtol(argv[2], NULL, 10);
        uint32_t runstop = strtol(argv[3], NULL, 10);
        return daqread_interval_combine(runstart, runstop);
	} else if (argc == 3) {
        uint32_t runstart = strtol(argv[1], NULL, 10);
        uint32_t runstop = strtol(argv[2], NULL, 10);
        return daqread_interval(runstart, runstop);
    } else if (argc == 2) {
        uint32_t run_number = strtol(argv[1], NULL, 10);
        return daqread(run_number);
    } else return -1;
}
