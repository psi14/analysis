
#include<TMath.h>

void fitenergy(char *runnumber){
	/* All Definitions and loading of the Data 
	*
	*
	*
	*/
    //defitions of variables
    char fpath[256];
    char fname[256];
    TH1F *h[7];
    TH1F *h_corr[7];
    TH1F *smoothh[7];
    TH1F *smoothh_corr[7];
	TH1F *h_sum_corr;
    Double_t integral_bl[14];
    Double_t ratios[7];
    Double_t zeroCrossing[7];
	Double_t offset[7];
	Double_t u_boundary;
	//define the length of the Histogram. You may change this when using more data
	int lengthHist=50000;// for 2900-3000 used 50000,for 2800-3200 used 50000
	int nBins=800;//for 2900-3000 used 500
    
    //Open File
	//Get Datapath from the datapath.txt
	// needs Files of Type: run_*_ana.root

    pathp = fopen("datapath.txt", "r");
    if (!pathp) {
        printf("Unable to read path to data file \n");
        return -1;
    }
    fgets(fpath, sizeof(fpath), pathp);
    sprintf(fname, "%sROOT/run_%s_ana.root", strtok(fpath, "\n"), runnumber);
    fclose(pathp);
	
    	//-------------------------------------------------------------------------------------------------------//
	/* First part of the function: 	-Create Histrogram of the Data of every Calorimeter. Plotted.
	*								-Create a Smooth Histogram of the Data (not Plotted)	
	*								-Get more or less the peak, low edge and first minima of the Histogram from the smoothed Histrogram
	*								-Use them as a Start value and fit a MichelSpectra convoluted with a Gaussian
	*								-Calculate the scaling Value, scale them and plot them into one
	*								
	*
	*/
	
	
	//Read the tree of the .. _ana_root
    TFile *f = new TFile(fname,"READ");
    TTree *t = f->Get("ana");
    //Open a Plotwindow
    TCanvas *c1 = new TCanvas("c1","Calo1",1500,1000 );
    c1->Divide(4,2);

    
    //fill histogram
    t->SetBranchAddress("integral",&integral_bl);
    //Set Calo encoding
    int calo[7];
    calo[0]=3;
    for(int i=1;i<7;i++){
        calo[i]=i+7;}
	// init all histograms
    for(int i=0;i<7;i++){
            char tit[80];
			char tit1[80];
            sprintf(tit,"Calo[%i] Integral",i+1,i+1);
			sprintf(tit1,"Calo[%i] Corr Integral",i+1,i+1);
            h[i]= new TH1F(tit,tit,nBins,0,lengthHist);
			h_corr[i]= new TH1F(tit1,tit1,nBins,0,80);
    }
	
	// Go over all Histograms and all entries
	// Fill the histograms, and fit Michel Gaus
	cout<<endl<<"------------------------- New Run ----------------------------------"<<endl<<endl;
	
    for(int i=0;i<7;i++){
		
		cout<<endl<<"------------------------- Cal "<<i<<" ----------------------------------"<<endl;
		//fill the Histrogram
        for(int ii=0;ii<t->GetEntries();ii++){
            t->GetEntry(ii);
            h[i]->Fill(integral_bl[calo[i]]);
        }
        
        //Create the smooth Histogram and calculate very rudimentary the Peak, first Minima and end of the low edge..
        smoothh[i]=smoothhist(h[i],17);
        smoothh[i]->SetLineColor(10);
        //Get the Peak of the histogram.. (Highest Point after an offset)
        	int peak=getPeak(smoothh[i],nBins*.04,smoothh[i]->GetXaxis()->GetXmax());
		//Get the first minima
        	int min =getMinima(smoothh[i],0,peak);
        //Very rudimentary function to get the end of the falling edge (The last number has to be increased/decreased wenn using more/less Datasets)
        	int low =findlow(smoothh[i],nBins*.0.08,lengthHist,0.16*smoothh[i]->GetBinContent(peak));//used 40,0.16 for 50000


		/*Fitting of the Convoluted-Michel Curve	
		*	Convolved Michel: (Done with mathematica,does not produce converging Taylor series, therefore I entered the whole shit into Root..)
		*	Startparameters are: 	p0: Peak hight
		*							p1: Stddev of Gaussian (0.1)
		*							p2; Offset of the whole function
		*							p3; Scaling of the whole function.. This +p2 is the sharp peak.. 52.8MeV
		*/
		TF1 *ConvMichel= new TF1("ConvMichel","[0]*(-1.5000000000000002*[1]^2*TMath::Erf((0.7071067811865476*(-1.+((x-[2])/[3])))/[1])+3.0000000000000004*[1]^2*((x-[2])/[3])*TMath::Erf((0.7071067811865476*(-1.+((x-[2])/[3])))/[1])-1.5000000000000002*((x-[2])/[3])^2*TMath::Erf((0.7071067811865476*(-1.+((x-[2])/[3])))/[1])+1.0000000000000002*((x-[2])/[3])^3*TMath::Erf((0.7071067811865476*(-1.+((x-[2])/[3])))/[1])+1.5000000000000002*[1]^2*TMath::Erf((0.7071067811865476*((x-[2])/[3]))/[1])-3.0000000000000004*[1]^2*((x-[2])/[3])*TMath::Erf((0.7071067811865476*((x-[2])/[3]))/[1])+1.5000000000000002*((x-[2])/[3])^2*TMath::Erf((0.7071067811865476*((x-[2])/[3]))/[1])-1.0000000000000002*((x-[2])/[3])^3*TMath::Erf((0.7071067811865476*((x-[2])/[3]))/[1]))+[4]",10000,smoothh[i]->GetXaxis()->GetBinCenter(low));
			//
			ConvMichel->SetParameters(smoothh[i]->GetBinContent(peak),0.1,0,1/0.82*smoothh[i]->GetXaxis()->GetBinCenter(peak),0);

			cout<<"Parameters: "<<smoothh[i]->GetBinContent(peak)<<" "<<0.1<<" "<<0<<" "<<1./smoothh[i]->GetBinCenter(peak)<<" "<<0;
			//ConvMichel->SetParameters(smoothh[i]->GetBinContent(peak),1,0,1./smoothh[i]->GetBinCenter(peak),0);
			ConvMichel->FixParameter(4,0);
			//ConvMichel->FixParameter(2,0);
			//ConvMichel->FixParameter(3,1);
			//ConvMichel->FixParameter(4,0);
			//Set Parameter Limits
			//ConvMichel->SetParLimits()

        	//Draw and fit everything:
        	c1->cd(i+1);
			// ConvMichel
			TFitResultPtr resultptr2=h[i]->Fit("ConvMichel","RS");
			gStyle->SetOptFit(1011);
			ConvMichel->Draw("Same");
		
		
		/* SCALING: This is done very rudimentary right now.. because I just use x=p2+p3.. 
		*
		*
		*/
       	zeroCrossing[i]=resultptr2->Parameter(3)+resultptr2->Parameter(2);
        ratios[i]=zeroCrossing[i]/52.8;
        cout<<"zeroCrossing "<<zeroCrossing[i]<<endl;
	

	}
	
	//-------------------------------------------------------------------------------------------------------//
	
	
	c1->cd(8);
	
	/* Fill the last Histrogram.. (Plot all the smoothed and scaleed Histograms together..)
	*  (I use smoothed here because it is easier to see the if they really fit)
	*
	*
	*/
	for(int i=0;i<7;i++){
		for(int ii=0;ii<t->GetEntries();ii++){
			t->GetEntry(ii);
            h_corr[i]->Fill((integral_bl[calo[i]])/ratios[i]);
		}
		smoothh_corr[i]=smoothhist(h_corr[i],17);
		
		smoothh_corr[i]->SetLineColor(i);		
		smoothh_corr[i]->Draw("SAME");
					
	}

	//---------------------------------------------------------------------------------------------------//
	/* Create a new Canvas and Histogram and Calculate the Sum of all the scaled Histograms
	*
	*
	*
	*/
	//Draw the sum 
	TCanvas *c2 = new TCanvas("c2","Calo Sum",1500,1000 );
	h_sum_corr=new TH1F("Calo Sum","Calo Sum",200,0,100);
	for(int ii=0;ii<t->GetEntries();ii++){
		Double_t intsum=0;
		t->GetEntry(ii);
			for(int i=0;i<7;i++){
				h_sum_corr->Fill(integral_bl[calo[i]]/ratios[i]);
			//intsum+=integral_bl[calo[i]]/ratios[i];
		}
		//h_sum_corr->Fill(intsum);
	}
		
		h_sum_corr->Draw();
		
		//Show the Michel function.. (in order to be sure that the function, written into root is the correct one)
	TCanvas *c3 = new TCanvas("c3","TestMichel",1500,1000);
	TF1 *ConvMichel1= new TF1("ConvMichel1","[0]*(-1.5000000000000002*[1]^2*TMath::Erf((0.7071067811865476*(-1.+((x-[2])/[3])))/[1])+3.0000000000000004*[1]^2*((x-[2])/[3])*TMath::Erf((0.7071067811865476*(-1.+((x-[2])/[3])))/[1])-1.5000000000000002*((x-[2])/[3])^2*TMath::Erf((0.7071067811865476*(-1.+((x-[2])/[3])))/[1])+1.0000000000000002*((x-[2])/[3])^3*TMath::Erf((0.7071067811865476*(-1.+((x-[2])/[3])))/[1])+1.5000000000000002*[1]^2*TMath::Erf((0.7071067811865476*((x-[2])/[3]))/[1])-3.0000000000000004*[1]^2*((x-[2])/[3])*TMath::Erf((0.7071067811865476*((x-[2])/[3]))/[1])+1.5000000000000002*((x-[2])/[3])^2*TMath::Erf((0.7071067811865476*((x-[2])/[3]))/[1])-1.0000000000000002*((x-[2])/[3])^3*TMath::Erf((0.7071067811865476*((x-[2])/[3]))/[1]))",0,30000);
	ConvMichel1->SetParameters(250,0.1,0,25000);
	ConvMichel1->Draw();
	
}

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
// Help Functions//

/* @descr: 	Smooth the Histogram. (Take n Value before and after calculate the mean)
 *
 * @param:  TH1F *h :                       Histogram to smooth
 *          const uint16_t width:           width of floating mean, should be uneven
 *
 *
 * @return *smothh:                   pointer to smoothed Histogram
 */
TH1F* smoothhist(const TH1F *h,const int width){
    
    TH1F* smoothh= new TH1F("Smooth Hist","Smooth Hist",h->GetNbinsX(),h->GetXaxis()->GetXmin(),h->GetXaxis()->GetXmax());
    for(int i=0;i<h->GetNbinsX();i++){
        Double_t buffer=0;
        int iii=0;
        for(int ii=-(width-1)/2;ii<=(width-1)/2;ii++){
            if((i+ii>=0)&&(i+ii<=h->GetNbinsX())){
                buffer+=h->GetBinContent(i+ii);
                iii++;
            }
        }
        buffer/=iii;
        smoothh->SetBinContent(i,buffer);
    }
    return smoothh;
}

/* @descr: calculate the maxima of the histogram in the userdefined range
 *
 * @param historgram, (if possible smoothed)
 * @param startpoint of range
 * @param endpoint of range
 *
 * @return maxima
 */


int getPeak(const TH1F *smoothh,const int startp,const int endpoint){
    Double_t maxVal=0;
    int bin=0;
    for(int i=startp;i<smoothh->GetNbinsX()&&i<endpoint;i++){
        if(smoothh->GetBinContent(i)>=maxVal){
            maxVal=smoothh->GetBinContent(i);
            bin=i;
        }
    }
    
    return bin;
    
}

/* @descr: calculate the minima of the histogram in the userdefined range
 *
 * @param historgram, (if possible smooted
 * @param startpoint of range
 * @param endpoint of range
 *
 * @return minima
 */

int getMinima(const TH1F *smoothh,const int startp,const int endpoint){
    Double_t minVal=smoothh->GetMaximum();
    int bin=0;
    for(int i=startp;i<smoothh->GetNbinsX()&&i<endpoint;i++){
        if(smoothh->GetBinContent(i)<=minVal){
            minVal=smoothh->GetBinContent(i);
            bin=i;
        }
    }
    
    return bin;
    
}

/* @descr: primitve function to calc the end the falling Edge of the Michelspectra
 *
 * @param historgram, (if possible smooted)
 * @param startpoint of range
 * @param endpoint of range
 * @the limit/boundary when the Michelspectra should end
 *
 * @return end of the falling Edge of the michelspectra
 */

int findlow(const TH1F *smoothh,const int startp,const int endpoint,const double limit){
    Double_t maxVal=0;
    int bin=0;
    for(int i=startp;i<smoothh->GetNbinsX()&&i<endpoint;i++){
        if(smoothh->GetBinContent(i)<=limit){
            maxVal=smoothh->GetBinContent(i);
            bin=i;
            return bin;
        }
    }
    
    return bin;
}



