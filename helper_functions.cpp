/**
 * PSI14 - helper_functions.cpp
 *
 * helper functions for analysis programs
 *
 * @author Carsten Grzesik <grzesik@physi.uni-heidelberg.de>
 * @author Sebastian Schenk <Sebastian.Schenk@stud.uni-heidelberg.de>
 */
 
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
// #include <unistd.h>
#include <TGraph.h>
#include <TF1.h>
#include <TString.h>
#include "helper_functions.hpp"

bool _triplexor(bool a, bool b, bool c)
{
    return ( (!a && (b ^ c)) || (a && !(b || c)) );
}

unsigned int _find_max(unsigned int nelements, uint16_t *array)
{
    uint16_t max = 0;
    uint16_t nmax = 0;
    for (unsigned int n = 0; n < nelements; n++) {
        if (array[n] > max) {
            max = array[n];
            nmax = n;
        }
    }
    
    return nmax;
}

unsigned int _find_min(unsigned int nelements, uint16_t *array)
{
    uint16_t min = 65535;
    uint16_t nmin = 0;
    for (unsigned int n = 0; n < nelements; n++) {
        if (array[n] < min) {
            min = array[n];
            nmin = n;
        }
    }
    
    return nmin;
}

double _baseline(const unsigned int nsamples, uint16_t *eventdata)
{
    double avg, sum = 0;
    for (unsigned int n = 0; n < nsamples; n++) {
        sum += eventdata[n];
    }
    avg = sum / nsamples;
    
    return avg;
}

double _baseline2(const unsigned int nsamples, uint16_t *eventdata, uint16_t nevents)
{

    double avg, sum = 0;
    for (unsigned int n = nevents; n > nevents-nsamples; n--) {
        sum += eventdata[n];
    }
    avg = sum / nsamples;
    
    return avg;
}

bool _bl(const unsigned int nsamples, const unsigned int center,
         const unsigned int lOffset, const unsigned int lPoints,
         const unsigned int rOffset, const unsigned int rPoints,
         const unsigned int polDeg, double *chisquare, unsigned int *ndf,
         double *fitparams, uint16_t *eventdata)
{
    TGraph fitgraph(lPoints+rPoints);

    unsigned int lStart, rStart;

    // check if arguments would result in index going out of bounds
    if ((long int)center-lOffset-lPoints+1<0 ||
        center+rOffset+rPoints>nsamples) {
        printf("_bl: Error - Range off-limits!\n");
        return false;
      }
    else {
      lStart = center-lOffset-lPoints+1;
      rStart = center+rOffset;
    }

    // fill TGraph with points left/right of center and fit with pol1
    for (unsigned int i = 0; i < lPoints+rPoints; ++i) {
      if (i < lPoints)
        fitgraph.SetPoint(i, lStart+i, eventdata[lStart+i]);
      else
        fitgraph.SetPoint(i, rStart+i-lPoints, eventdata[rStart+i-lPoints]);
    }

    fitgraph.Fit(TString::Format("pol%u", polDeg), "Q");
  
    // write resulting fit parameters into array
    TF1 *myfit = (TF1*) fitgraph.GetFunction(TString::Format("pol%u", polDeg));
  
    for (unsigned int i = 0; i <= polDeg; ++i)
        fitparams[i] = myfit->GetParameter(i);
    
	*chisquare = myfit->GetChisquare();
    *ndf = myfit->GetNDF();

    return true;
}

double _sum(const unsigned int nsamples, uint16_t *eventdata)
{
    double sum = 0;
    for (unsigned int i = 0; i < nsamples; i++) {
        sum += eventdata[i];
    }
    
    return sum;
}

double _integral(const unsigned int nsamples, const double baseline,
                 uint16_t *eventdata)
{
    double totint = _sum(nsamples, eventdata);
    
    return totint - nsamples * baseline;
}

double _positive_integral(const unsigned int nsamples, const double baseline,
                          uint16_t *eventdata)
{
    double sum = 0;
    for (unsigned int i=0; i < nsamples; i++)
    {
        if(eventdata[i] > baseline) sum += eventdata[i] - baseline;
    }
    return sum;
}

double _negative_integral(const unsigned int nsamples, const double baseline,
                 uint16_t *eventdata)
{
    double sum = 0;
    for (unsigned int i=0; i < nsamples; i++)
    {
        if(eventdata[i] < baseline) sum += eventdata[i] - baseline;
    }
    return sum;
}

unsigned int _find_rising_edge(const unsigned int nsamples, const double baseline,
                               const double relthreshold, uint16_t *eventdata)
{
    unsigned int nmax = _find_max(nsamples, eventdata);
    int16_t max = eventdata[nmax] - baseline;
    int16_t threshold = relthreshold * max;
    unsigned int n = 0;
    while (eventdata[n]-baseline < threshold) n++;
    
    return n;
}

unsigned int _find_falling_edge(const unsigned int nsamples, const double baseline,
                                const double relthreshold, uint16_t *eventdata)
{
    unsigned int nmin = _find_min(nsamples, eventdata);
    int16_t min = eventdata[nmin] - baseline;
    int16_t threshold = relthreshold * min;
    unsigned int n = 0;
    while (eventdata[n]-baseline > threshold) n++;
    
    return n;

}

signed int _integral_interval(const unsigned int nstart, const unsigned int nstop,
                              const double baseline, uint16_t *eventdata)
{
    int16_t sum = 0;
    for (unsigned int n = nstart; n <= nstop; n++)
    {
        sum += eventdata[n] - baseline;
    }

    return sum;
}

double _integral_bl(const unsigned int nstart, const unsigned int nstop,
                    const unsigned int polDeg, double *fitparams,
                    uint16_t *eventdata)
{
    double sum = 0;

    // divide baseline contribution to reduce rounding errors
    double baselinecontrib = 0;

    for (unsigned int i = 0; i <= polDeg; ++i) {
        baselinecontrib += fitparams[i]/(i+1) * (pow(nstop+1/2., i+1)-pow(nstart-1/2., i+1));
    }

    baselinecontrib /= (1+nstop-nstart);

    for (unsigned int n = nstart; n <= nstop; n++)
    {
        sum += eventdata[n]-baselinecontrib;
    }

    return sum;
}

double _get_bl(const unsigned int nstart, const unsigned int nstop,
               const unsigned int polDeg, double *fitparams,
               uint16_t *eventdata)
{
    double baselinecontrib = 0;

    for (unsigned int i = 0; i <= polDeg; ++i) {
        baselinecontrib += fitparams[i]/(i+1) * (pow(nstop+1/2., i+1)-pow(nstart-1/2., i+1));
    }

    return baselinecontrib;
}

double _entropy_bl(const unsigned int nstart, const unsigned int nstop,
                   const unsigned int polDeg, double *fitparams,
                   uint16_t *eventdata)
{
    double logsum = 0;
    double entropy = 0;
    double corrBin = 0;
    unsigned int squaresum = 0;

    for (unsigned int i = nstart; i <= nstop; ++i) {
        corrBin = _integral_bl(i, i, polDeg, fitparams, eventdata);

		if (corrBin != 0) {
            logsum += pow(corrBin, 2) * log(pow(corrBin, 2));
            squaresum += pow(corrBin, 2);
        }
    }

    if (squaresum != 0) {
        entropy = log(squaresum) - logsum/squaresum;
    } else {
        entropy = 1;
    }

    return entropy;
}

signed int _find_peak(const unsigned int nsamples, const unsigned int polDeg,
                      double *fitparams, const long int threshold,
                      unsigned int *peakdata, const unsigned int startpos,
                      uint16_t *eventdata)
{
    // define flags and variables for peak search
    short int peakfound = 0;
    short int passedthr = 0;
    short int overthr = 0;
    
    long int maxormin = 0;
    unsigned int nmaxormin = 0;
    unsigned int nlthr = 0;
    unsigned int nrthr = 0;

    double corrBin;
    
    // search for peak
    for (unsigned int i = startpos; i < nsamples; ++i) {
        corrBin = _integral_bl(i, i, polDeg, fitparams, eventdata);

        // check if threshold has been passed
        if (threshold >= 0) {
            passedthr = (threshold <= corrBin);
        } else {
            passedthr = (threshold >= corrBin);
        }
        
        // if threshold is passed, determine minimum/maximum value
        // as long as samples are over/under threshold
        if (passedthr&&overthr) {
            if ( (threshold>=0 && maxormin<eventdata[i]) ||
                 (threshold<0 && maxormin>eventdata[i])     ) {
                maxormin = eventdata[i];
                nmaxormin = i;
            }
        } else if (passedthr) {
            overthr = 1;
            maxormin = eventdata[i];
            nmaxormin = i;
        } else if (overthr) {
            peakfound = 1;
            nrthr = i;
            break;
        } else {
            nlthr = i;
        }
    }
    
    // only write to storage array when peak has been found
    if (!peakfound) {
        return -1;
    } else {
        peakdata[0] = nmaxormin;
        peakdata[1] = nlthr;
        peakdata[2] = nrthr;
        return 0;
    }
}

signed int _find_peak_edges(const unsigned int nsamples,
                            const unsigned int polDeg, double *fitparams,
                            const double relthreshold,
                            const unsigned int npeak, unsigned int *peakdata,
                            uint16_t *eventdata)
{
    // check validity of parameters
    if (npeak>nsamples || relthreshold<0 || relthreshold>1) {
        return -2;
    }
    
    // define variables for the search
    double corrBin;
    long int relmaxormin = _integral_bl(npeak, npeak, polDeg, fitparams,
                                        eventdata);
    unsigned int nlthr = npeak;
    unsigned int nrthr = npeak;
    
    // check for first bins on both sides which are below threshold
    // whether peak is treated as positive or negative is determined
    // by whether its height is above or below the baseline
    for (long int i = (long int)npeak-1; i >= 0; --i) {
        nlthr = i;
        corrBin = _integral_bl(i, i, polDeg, fitparams, eventdata);

        if ( (relmaxormin>0&&corrBin< (relthreshold*relmaxormin)) ||
             (relmaxormin<0&&corrBin> (relthreshold*relmaxormin)) )
            break;
    }
    
    for (unsigned int i = npeak+1; i < nsamples; ++i) {
        nrthr = i;
        corrBin = _integral_bl(i, i, polDeg, fitparams, eventdata);
        
        if ( (relmaxormin>0&&corrBin<(relthreshold*relmaxormin)) ||
             (relmaxormin<0&&corrBin>(relthreshold*relmaxormin)) )
            break;        
    }
    
    // write to storage array
    peakdata[0] = nlthr;
    peakdata[1] = nrthr;

    return 0;
}

void _smooth_wave(const unsigned int nsamples, const unsigned int range,
                  uint16_t *eventdata, uint16_t *target)
{
    unsigned int actualrange = 0;
    long unsigned int mean = 0;
    
    for (unsigned int i = 0; i < nsamples; ++i) {
        mean = 0;
        
        // if data points too close to waveform borders, use maximum
        // range possible, otherwise use specified range
        if (i<range || nsamples-i-1<range) {
            if (i>=range) {
                actualrange = nsamples-i-1;
            } else {
                actualrange = i;
            }
        } else {
            actualrange = range;
        }
        
        // compute mean of interval around datapoint (including the
        // data point itself)
        for (long int j = i-actualrange; j <= i+actualrange; ++j) {
            mean += eventdata[j];
        }
        mean /= (2*actualrange+1);
        target[i] = mean;
    }
}
