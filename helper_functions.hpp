/**
 * PSI14 - helper_functions.hpp
 *
 * helper functions for analysis programs
 *
 * @author Carsten Grzesik <grzesik@physi.uni-heidelberg.de>
 * @author Sebastian Schenk <Sebastian.Schenk@stud.uni-heidelberg.de>
 */

#include <stdint.h>

/**
 * @brief bool operation that is true for only one of the inputs being true
 *
 * @param[in] a     bool input
 * @param[in] b     bool input
 * @param[in] c     bool input
 * @return true for only one input being true, otherwise false
 */
bool _triplexor(bool a, bool b, bool c);

/**
 * @brief find the index of the maximum value of an array
 *
 * @warning this function assumes that the array only contains positive values
 *
 * @param[in] nelements number of elements in the array
 * @param[in] array     1D array
 * @return index of the maximum value entry
 */
unsigned int _find_max(unsigned int nelements, uint16_t *array);

/**
 * @brief find the index of the minimum value of an array
 *
 * @param[in] nelements number of elements in the array
 * @param[in] array     1D array
 * @return index of the minimum value entry
 */
unsigned int _find_min(unsigned int nelements, uint16_t *array);

/**
 * @brief calculate the pulse baseline of an event shape by averaging over
 *        the first few samples
 *
 * @warning this function assumes that the pulse has its rising/falling edge
 *          at least after the given samples
 *
 * @param[in] nsamples  number of samples to be averaged over
 * @param[in] eventdata array containing the data of the event
 * @return value of the baseline
 */
double _baseline(const unsigned int nsamples, uint16_t *eventdata);

/**
 * @brief calculate the pulse baseline of an event shape by averaging over
 *        the last few samples
 *
 * @warning this function assumes that the pulse has its rising/falling edge
 *          at least after the given samples
 *
 * @param[in] nsamples  number of samples to be averaged over
 * @param[in] eventdata array containing the data of the event
 * @return value of the baseline
 */
double _baseline2(const unsigned int nsamples, uint16_t *eventdata, uint16_t nevents);

/**
 * @brief estimate the baseline by doing a polynomial fit
 *
 * @param[in] nsamples  number of the event samples
 * @param[in] center index (for ex. peak position)
 * @param[in] lOffset index offset of fit area left of center
 * @param[in] lPoints size of fit area left of center
 * @param[in] rOffset index offset of fit area right of center
 * @param[in] rPoints size of fit area right of center
 * @param[in] polDeg degree of polynomial used with _bl
 * @param[in] chisquare pointer to variable storing chisquare of fit
 * @param[in] ndf pointer to variable storing no. of degrees of freedom
 *            of fit
 * @param[in] fitparams pointer to first element of array with
 *                      the same size as the value of polDeg;
 *                      fit parameters of baseline fit will be
 *                      stored in there
 * @param[in] eventdata array containing the data of the event
 * @return boolean indicating whether index would get out of bounds or not
 */
bool _bl(const unsigned int nsamples, const unsigned int center,
         const unsigned int lOffset, const unsigned int lPoints,
         const unsigned int rOffset, const unsigned int rPoints,
         const unsigned int polDeg, double *chisquare, unsigned int *ndf,
         double *fitparams, uint16_t *eventdata);

/**
 * @brief calculate the total sum of an event shape by summing up its samples
 *
 * @param[in] nsamples  number of the event samples
 * @param[in] eventdata array containing the data of the event
 * @return sum
 */
double _sum(const unsigned int nsamples, uint16_t *eventdata);

/**
 * @brief calculate the integral of an event shape
 *
 * this function already subtracts the baseline to calculate the integral
 *
 * @param[in] nsamples  number of event samples
 * @param[in] baseline  value of the pulse baseline
 * @param[in] eventdata array containing the data of the event
 * @return integral
 */
double _integral(const unsigned int nsamples, const double baseline,
                 uint16_t *eventdata);

/**
 * @brief calculate the integral of an event shape by summing up all values
 *        bigger than the baseline
 *
 * @param[in] nsamples  number of event samples
 * @param[in] baseline  value of the pulse baseline
 * @param[in] eventdata array containing the data of the event
 * @return positive integral
 */
double _positive_integral(const unsigned int nsamples, const double baseline,
                          uint16_t *eventdata);

/**
 * @brief calculate the integral of an event shape by summing up all values
 *        smaller than the baseline
 *
 * @param[in] nsamples  number of event samples
 * @param[in] baseline  value of the pulse baseline
 * @param[in] eventdata array containing the data of the event
 * @return negative integral
 */
double _negative_integral(const unsigned int nsamples, const double baseline,
                          uint16_t *eventdata);

/**
 * @brief find the rising edge of an event shape by a constant fraction wrt maximum
 *
 * @param[in] nsamples     number of event samples
 * @param[in] baseline     value of the pulse baseline
 * @param[in] relthreshold relative threshold for constant fraction
 * @param[in] eventdata    array containing the data of the event
 * @return index of sample for the rising edge
 */
unsigned int _find_rising_edge(const unsigned int nsamples, const double baseline,
                               const double relthreshold, uint16_t *eventdata);

/**
 * @brief find the rising edge of an event shape by a constant fraction wrt minimum
 *
 * @param[in] nsamples     number of event samples
 * @param[in] baseline     value of the pulse baseline
 * @param[in] relthreshold relative threshold for constant fraction
 * @param[in] eventdata    array containing the data of the event
 * @return index of sample for the falling edge
 */
unsigned int _find_falling_edge(const unsigned int nsamples, const double baseline,
                                const double relthreshold, uint16_t *eventdata);

/**
 * @brief calculate the integral in an interval
 *
 * @warning nstart has to be smaller than nstop
 *
 * @param[in] nstart    index of starting sample
 * @param[in] nstop     index of stopping sample
 * @param[in] baseline  value of the pulse baseline
 * @param[in] eventdata array containing the data of the event
 * @return integral between nstart and nstop
 */
signed int _integral_interval(const unsigned int nstart, const unsigned int nstop,
                              const double baseline, uint16_t *eventdata);

/**
 * @brief integrate using the baseline estimate from _bl
 *
 * @warning nstart has to be smaller than nstop
 *
 * @param[in] nstart    index of starting sample
 * @param[in] nstop     index of stopping sample
 * @param[in] polDeg degree of polynomial used with _bl
 * @param[in] fitparams pointer to first element of array used with _bl
 * @param[in] eventdata array containing the data of the event
 * @return integral between nstart and nstop with baseline subtracted
 */
double _integral_bl(const unsigned int nstart, const unsigned int nstop,
                    const unsigned int polDeg, double *fitparams,
                    uint16_t *eventdata);

/**
 * @brief returns the baseline contribution within an interval
 *        using the baseline estimate from _bl
 *
 * @warning nstart has to be smaller than nstop
 *
 * @param[in] nstart    index of starting sample
 * @param[in] nstop     index of stopping sample
 * @param[in] polDeg degree of polynomial used with _bl
 * @param[in] fitparams pointer to first element of array used with _bl
 * @param[in] eventdata array containing the data of the event
 * @return baseline contribution of integral between nstart and nstop
 */
double _get_bl(const unsigned int nstart, const unsigned int nstop,
               const unsigned int polDeg, double *fitparams,
               uint16_t *eventdata);

/**
 * @brief calculate interval entropy using the baseline estimate from _bl
 *
 * @warning nstart has to be smaller than nstop
 *
 * @param[in] nstart    index of starting sample
 * @param[in] nstop     index of stopping sample
 * @param[in] polDeg degree of polynomial used with _bl
 * @param[in] fitparams pointer to first element of array used with _bl
 * @param[in] eventdata array containing the data of the event
 * @return baseline corrected entropy between nstart and nstop
 */
double _entropy_bl(const unsigned int nstart, const unsigned int nstop,
                   const unsigned int polDeg, double *fitparams,
                   uint16_t *eventdata);

/**
 * @brief find a peak within the event data
 *
 * @param[in] nsamples  number of event samles
 * @param[in] polDeg degree of polynomial used with _bl
 * @param[in] fitparams pointer to first element of array used with _bl
 * @param[in] threshold threshold a peak must pass; the sign
 *                      determines whether positive or negative
 *                      peaks are looked for
 * @param[in] peakdata  pointer to array of size >=3 to store peak
 *                      related data; 0 - index of peak maximum;
 *                      1 - first bin on left side which is below
 *                      threshold; 2 - first bin on right side which
 *                      is below threshold
 * @param[in] startpos starting index of peak search 
 * @param[in] eventdata array containing the data of the event
 * @return 0 if peak has been found, otherwise -1
 */
signed int _find_peak(const unsigned int nsamples, const unsigned int polDeg,
                      double *fitparams, const long int threshold,
                      unsigned int *peakdata, const unsigned int startpos,
                      uint16_t *eventdata);

/**
 * @brief find first bins on both sides of a peak below/above a
 *        relative threshold
 * 
 * @warning experimental, needs to be tested
 *
 * @param[in] nsamples     number of event samles
 * @param[in] polDeg degree of polynomial used with _bl
 * @param[in] fitparams pointer to first element of array used with _bl
 * @param[in] relthreshold fraction of peak height used for threshold
 * @param[in] npeak        index of peak maximum
 * @param[in] peakdata     pointer to array of size >=2 to store peak
 *                         related data; 0 - index of peak maximum;
 *                         0 - first bin on left side which is below
 *                         threshold; 1 - first bin on right side which
 *                         is below threshold
 * @param[in] eventdata    array containing the data of the event
 * @return 0 if at least one of the edge indices is different from
 *         the peak index, otherwise -1, returns -2 for invalid
 *         parameters
 */
signed int _find_peak_edges(const unsigned int nsamples,
                            const unsigned int polDeg, double *fitparams,
                            const double relthreshold,
                            const unsigned int npeak, unsigned int *peakdata,
                            uint16_t *eventdata);

/**
 * @brief smooth event data (central floating mean)
 *
 * @param[in] nsamples  number of event samles
 * @param[in] range     size of half of the interval around a data
 *                      point of which to take the mean of (excluding
 *                      the data point itself)
 * @param[in] eventdata array containing the data of the event
 * @param[in] target    array with the same size as eventdata for
 *                      storage of the smoothed out waveform
 * @return nothing
 */
void _smooth_wave(const unsigned int nsamples, const unsigned int range,
                  uint16_t *eventdata, uint16_t *target);
