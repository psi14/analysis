CC      = g++
CFLAGS  = `root-config --cflags` -g
LDFLAGS = `root-config --glibs`

all: daqread daqana viewevents

daqread: daqread.o helper_functions.o
	$(CC) $(CFLAGS)  $^ -o $@ $(LDFLAGS)

daqana: daqana.o helper_functions.o
	$(CC) $(CFLAGS)  $^ -o $@ $(LDFLAGS)

viewevents: viewevents.o helper_functions.o
	$(CC) $(CFLAGS)  $^ -o $@ $(LDFLAGS)
	
helper_functions.o: helper_functions.cpp
	$(CC) $(CFLAGS) -c $^

daqread.o: daqread.cpp
	$(CC) $(CFLAGS) -c $^	
	
daqana.o: daqana.cpp
	$(CC) $(CFLAGS) -c $^
	
viewevents.o: viewevents.cpp
	$(CC) $(CFLAGS) -c $^
	
clean:
	rm daqread
	rm daqana
	rm viewevents