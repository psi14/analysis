void pistop(int runnumber){
  
  char fpath[256];
    char fname[256];
    pathp = fopen("datapath.txt", "r");
    if (!pathp) {
        printf("Unable to read path to data file \n");
        return -1;
    }
    fgets(fpath, sizeof(fpath), pathp);
    sprintf(fname, "%sROOT/run_%u_ana.root", strtok(fpath, "\n"), runnumber);
    fclose(pathp);

TFile *f = new TFile(fname,"READ");
TTree *t = f->Get("ana");

TCanvas *c2 = new TCanvas("c2","PiStop",600,1500);
c2->Divide(1,3);

c2->cd(1);

t->Draw("(256-falling_edge[2])*10>>h_fedge_p(100,0,1024)","falling_edge[2]>150 && falling_edge[2]<300"); //,"falling_edge[2]>150 && falling_edge[2]<300"
    
    TH1F *h_fedge_p = (TH1F*)gDirectory->Get("h_fedge_p");

    h_fedge_p->GetXaxis()->SetTitle("falling_edge_pistop");
    h_fedge_p->GetYaxis()->SetTitle("counts");
    
    
    h_fedge_p->Draw();
    
//     c2->cd(2);
// 
// t->Draw("falling_edge[4]>>h_fedge_scint1dis(100,-50,350)");
//     
//     TH1F *h_fedge_scint1dis = (TH1F*)gDirectory->Get("h_fedge_scint1dis");
// 
//     h_fedge_scint1dis->GetXaxis()->SetTitle("falling_edge_scint1dis");
//     h_fedge_scint1dis->GetYaxis()->SetTitle("counts");
//     
//     
//     h_fedge_scint1dis->Draw();
    
    c2->cd(2);
    t->Draw("max[0]-baseline[0]:(256-falling_edge[2])*10>>h_fedge_scatter(100,0,1024,10000,-1000,9000)","falling_edge[2]>150 && falling_edge[2]<300");
    TH1F *h_fedge_scatter = (TH1F*)gDirectory->Get("h_fedge_scatter");
//     h_integral_red->SetLineColor(kRed);
    
    h_fedge_scatter->GetXaxis()->SetTitle("falling_edge_pistop");
    h_fedge_scatter->GetYaxis()->SetTitle("TAC1");
    
    h_fedge_scatter->Draw();
    
    c2->cd(3);
    t->Draw("min[5]-baseline[5]:(256-falling_edge[2])*10>>h_fedge_scatter2(100,0,1024,10000,-9000,1000)","falling_edge[2]>150 && falling_edge[2]<300");
    TH1F *h_fedge_scatter2 = (TH1F*)gDirectory->Get("h_fedge_scatter2");
//     h_integral_red->SetLineColor(kRed);
    
    h_fedge_scatter2->GetXaxis()->SetTitle("falling_edge_pistop");
    h_fedge_scatter2->GetYaxis()->SetTitle("TAC2");
    
    h_fedge_scatter2->Draw();

}