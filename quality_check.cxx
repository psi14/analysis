void quality_check(char *runnumber){
    
    char fpath[256];
    char fname[256];
    pathp = fopen("datapath.txt", "r");
    if (!pathp) {
        printf("Unable to read path to data file \n");
        return -1;
    }
    fgets(fpath, sizeof(fpath), pathp);
    sprintf(fname, "%sROOT/run_%s_ana.root", strtok(fpath, "\n"), runnumber);
    fclose(pathp);

TFile *f = new TFile(fname,"READ");
TTree *t = f->Get("ana");

TCanvas *c1 = new TCanvas("c1","Calo1",2000,1500);
c1->Divide(7,3);

// int max_limitUp[7]={2000,9000,8000,2000,2000,2000,2000};
// int integral_limitUp[7]		;
// int integral_limitLow[7]	;


  for(int i=8; i<=13; i++){
    
    c1->cd(i-6);

    t->Draw(Form("max[%i]-baseline[%i]>>h_max_%i(100,-200,1800)",i,i,i));
    
    TH1F *h_max = (TH1F*)gDirectory->Get(Form("h_max_%i",i));

    h_max->GetXaxis()->SetTitle("max");
    h_max->GetYaxis()->SetTitle("counts");
    
    
    h_max->Draw();
    
    c1->cd(i+1);
    
    

    t->Draw(Form("integral[%i]>>h_integral_%i(200,-200000,200000)",i,i));
    
    TH1F *h_integral = (TH1F*)gDirectory->Get(Form("h_integral_%i",i));

    h_integral->GetXaxis()->SetTitle("integral");
    h_integral->GetYaxis()->SetTitle("counts");
    
    h_integral->Draw();
    
    
    c1->cd(i+8);

    t->Draw(Form("max[%i]-baseline[%i]:integral[%i]>>h_scatter_%i(200,-200000,200000,100,-200,1800)",i,i,i,i));
    TH1F *h_scatter = gDirectory->Get(Form("h_scatter_%i",i));

    h_scatter->GetXaxis()->SetTitle("integral");
    h_scatter->GetYaxis()->SetTitle("max");

    // TH1F *h_scatter = gDirectory->Get("h_scatter");
    // TH1F *h_scatter = (TH1F*)gDirectory->Get("h_scatter");
    h_scatter->Draw();

    c1->cd(i+1);
    t->Draw(Form("integral[%i]>>h_integral_red_%i(200,-200000,200000)",i,i),Form("max[%i]>8400",i),"SAME");
    TH1F *h_integral_red = (TH1F*)gDirectory->Get(Form("h_integral_red_%i",i));
    h_integral_red->SetLineColor(kRed);
    
    h_integral_red->GetXaxis()->SetTitle("integral");
    h_integral_red->GetYaxis()->SetTitle("counts");
    h_integral_red->Draw("SAME");

  }
  
  int i = 3;
  
  c1->cd(1);

    t->Draw(Form("max[%i]-baseline[%i]>>h_max_%i(100,-200,1800)",i,i,i));
    
    TH1F *h_max = (TH1F*)gDirectory->Get(Form("h_max_%i",i));

    h_max->GetXaxis()->SetTitle("max");
    h_max->GetYaxis()->SetTitle("counts");
    
    h_max->Draw();
    
    c1->cd(8);

    t->Draw(Form("integral[%i]>>h_integral_%i(200,-200000,200000)",i,i));
    
    TH1F *h_integral = (TH1F*)gDirectory->Get(Form("h_integral_%i",i));

    h_integral->GetXaxis()->SetTitle("integral");
    h_integral->GetYaxis()->SetTitle("counts");
    
    h_integral->Draw();
    
    c1->cd(15);

    t->Draw(Form("max[%i]-baseline[%i]:integral[%i]>>h_scatter_%i(200,-200000,200000,100,-200,1800)",i,i,i,i));
    TH1F *h_scatter = (TH1F*)gDirectory->Get(Form("h_scatter_%i",i));

    h_scatter->GetXaxis()->SetTitle("integral");
    h_scatter->GetYaxis()->SetTitle("max");

    // TH1F *h_scatter = gDirectory->Get("h_scatter");
    // TH1F *h_scatter = (TH1F*)gDirectory->Get("h_scatter");
    h_scatter->Draw();

    c1->cd(8);
    t->Draw(Form("integral[%i]>>h_integral_red_%i(200,-200000,200000)",i,i),Form("max[%i]>8400",i),"SAME");
    TH1F *h_integral_red = (TH1F*)gDirectory->Get(Form("h_integral_red_%i",i));
    h_integral_red->SetLineColor(kRed);
    
    h_integral_red->GetXaxis()->SetTitle("integral");
    h_integral_red->GetYaxis()->SetTitle("counts");
    h_integral_red->Draw("SAME");
    
    TCanvas *c2 = new TCanvas("c2","PiStop",1500,1500);
    c2->Divide(3,3);

    c2->cd(1);

    t->Draw("(256-falling_edge[2])*10>>h_fedge_p(102,0,1024)","falling_edge[2]>150 && falling_edge[2]<300"); //,"falling_edge[2]>150 && falling_edge[2]<300"
    
    TH1F *h_fedge_p = (TH1F*)gDirectory->Get("h_fedge_p");

    h_fedge_p->GetXaxis()->SetTitle("falling_edge_pistop [ns]");
    h_fedge_p->GetYaxis()->SetTitle("counts");
    
    
    h_fedge_p->Draw();
    TF1 *fitf = new TF1("twoexp","[0]*(1-exp(-(x-[1])/[2]))*exp(-(x-[1])/[3])+[4]",0,1000); 
    fitf->SetParNames("norm","offset","tau_pi","tau_mu","bkg");
    fitf->SetParameters(10,200,26,2200,700);
    fitf->SetParLimits(2,10,50);
    fitf->SetParLimits(3,1000,4000);
    fitf->FixParameter(2,26);
    fitf->FixParameter(3,2200);
    h_fedge_p->Fit(fitf,"","",130,1000);
    
//     c2->cd(2);
// 
// t->Draw("falling_edge[4]>>h_fedge_scint1dis(100,-50,350)");
//     
//     TH1F *h_fedge_scint1dis = (TH1F*)gDirectory->Get("h_fedge_scint1dis");
// 
//     h_fedge_scint1dis->GetXaxis()->SetTitle("falling_edge_scint1dis");
//     h_fedge_scint1dis->GetYaxis()->SetTitle("counts");
//     
//     
//     h_fedge_scint1dis->Draw();
    
    c2->cd(4);
    t->Draw("min[0]-baseline[0]:(256-falling_edge[2])*10>>h_fedge_scatter(100,0,1024,10000,-9000,1000)","falling_edge[2]>150 && falling_edge[2]<300");
    TH1F *h_fedge_scatter = (TH1F*)gDirectory->Get("h_fedge_scatter");
//     h_integral_red->SetLineColor(kRed);
    
    h_fedge_scatter->GetXaxis()->SetTitle("falling_edge_pistop");
    h_fedge_scatter->GetYaxis()->SetTitle("TAC1");
    
    h_fedge_scatter->Draw();
    
    c2->cd(7);
    t->Draw("min[5]-baseline[5]:(256-falling_edge[2])*10>>h_fedge_scatter2(100,0,1024,10000,-9000,1000)","falling_edge[2]>150 && falling_edge[2]<300");
    TH1F *h_fedge_scatter2 = (TH1F*)gDirectory->Get("h_fedge_scatter2");
//     h_integral_red->SetLineColor(kRed);
    
    h_fedge_scatter2->GetXaxis()->SetTitle("falling_edge_pistop");
    h_fedge_scatter2->GetYaxis()->SetTitle("TAC2");
    
    h_fedge_scatter2->Draw();

    
    c2->cd(5);

    t->Draw("min[0]-baseline[0]>>h_tac1(101,-8000,-1000)"); //,"falling_edge[2]>150 && falling_edge[2]<300"
    
    TH1F *h_tac1 = (TH1F*)gDirectory->Get("h_tac1");

    h_tac1->GetXaxis()->SetTitle("TAC1 [ADC]");
    h_tac1->GetYaxis()->SetTitle("counts");
    
    
    h_tac1->Draw();
    
    c2->cd(8);

    t->Draw("min[5]-baseline[5]>>h_tac2(70,-8000,-1500)"); //,"falling_edge[2]>150 && falling_edge[2]<300"
    
    TH1F *h_tac2 = (TH1F*)gDirectory->Get("h_tac2");

    h_tac2->GetXaxis()->SetTitle("TAC2 [ADC]");
    h_tac2->GetYaxis()->SetTitle("counts");
    
    
    h_tac2->Draw();
    
    c2->cd(6);
    t->Draw("min[0]-baseline[0]:min[5]-baseline[5]>>h_tac_scatter(101,-8000,0,101,-8000,0)");
    TH1F *h_tac_scatter = (TH1F*)gDirectory->Get("h_tac_scatter");
//     h_integral_red->SetLineColor(kRed);
    
    h_tac_scatter->GetXaxis()->SetTitle("TAC2");
    h_tac_scatter->GetYaxis()->SetTitle("TAC1");
    
    h_tac_scatter->Draw();

    
    
    c2->cd(2);
    t->Draw("(rising_edge[1]-rising_edge[6])*10>>h_tof_5_1(121,-200,1000)");
    TH1F *h_tof_5_1 = (TH1F*)gDirectory->Get("h_tof_5_1");
//     h_integral_red->SetLineColor(kRed);
    
    h_tof_5_1->GetXaxis()->SetTitle("delta t [ns]");
    h_tof_5_1->GetYaxis()->SetTitle("counts");
    
    h_tof_5_1->Draw();
    
    TF1 *fitf2 = new TF1("twoexp2","[0]*(1-exp(-(x-[1])/[2]))*exp(-(x-[1])/[3])+[4]",0,1000); 
    fitf2->SetParNames("norm2","offset2","tau_pi2","tau_mu2","bkg2");
    fitf2->SetParameters(1000,50,26,2200,1100);
    fitf2->SetParLimits(2,10,50);
    fitf2->SetParLimits(3,500,3000);
    //fitf2->FixParameter(2,26);
    //fitf2->FixParameter(3,2200);
    h_tof_5_1->Fit(fitf2,"","",60,900);

    c2->cd(3);
    t->Draw("256-falling_edge[2]*10:(rising_edge[1]-rising_edge[6])*10>>h_scattertime(120,-1000,4000,120,-2500,100)");

    TH1F *h_scattertime = (TH1F*)gDirectory->Get("h_scattertime");
    h_scattertime->GetXaxis()->SetTitle("delta-t [ns]");
    h_scattertime->GetYaxis()->SetTitle("pi-stop [ns]");

    h_scattertime->Draw();

    TCanvas *c3= new TCanvas("c3","c3", 1000,1500);
    c3->Divide(1,3);

    c3->cd(1);
    t->Draw("rising_edge[4]-rising_edge[7]>>h_Scintillator1offset(100,-1000,1000");

    TH1F * h_Scintillator1offset = (TH1F*)gDirectory->Get("h_Scintillator1offset");

    h_Scintillator1offset->GetXaxis()->SetTitle("Scintillator Box 1 - Scintillator Box 2");
    h_Scintillator1offset->GetYaxis()->SetTitle("Counts");

    c3->cd(1)->SetLogy();

    h_Scintillator1offset->Draw();

    c3->cd(2);
    c3->cd(2)->SetLogy();
    t->Draw("rising_edge[4]-falling_edge[2]>>h_box1_sync(401,-200,200)");
    TH1F *h_box1_sync = (TH1F*)gDirectory->Get("h_box1_sync");

    h_box1_sync->GetXaxis()->SetTitle("disri1 - pistop");
    h_box1_sync->GetYaxis()->SetTitle("counts");

    h_box1_sync->Draw();

    c3->cd(3);
    c3->cd(3)->SetLogy();
    t->Draw("rising_edge[7]-falling_edge[2]>>h_box2_sync(401,-200,200)");
    TH1F *h_box2_sync = (TH1F*)gDirectory->Get("h_box2_sync");

    h_box2_sync->GetXaxis()->SetTitle("disri1 - pistop");
    h_box2_sync->GetYaxis()->SetTitle("counts");

    h_box2_sync->Draw();




    //ReadCalo file



    Double_t max[14];
    t->SetBranchAddress("max",&max);
    Double_t baseline[14];
    t->SetBranchAddress("baseline",&baseline);
    Double_t falling_edge[14];
    t->SetBranchAddress("falling_edge",&falling_edge);


    // - - - - - - - - - - - - - -
    Double_t   calo[7], calsum, calsumcorr;
    Double_t scaling[7]={1,1.696,0.896,1.696,1.13,0.979,1.067};


    // Histograms
    TH1F *h_sum=new TH1F("CaloSum","CaloSum",500,-100,15000);
    TH1F *h_sum_corr=new TH1F("CaloSumCorr","CaloSumCorr",500,-100,15000);
    TH1F *h_time = new TH1F("Pi Stop","PiStop",100,0,1000);
    TH1F *h_time_cut = new TH1F("Pi Stop Cut","Pi Stop Cut",100,0,1000);
    TH2F *h_time_ene = new TH2F("Time vs EnergyTotal","Time vs EnergyTotal;Time;Energy;",100,0,1000,500,-100,15000);
    h_sum_corr->SetLineColor(2);

    TH1F *h[7];
    TH1F *h_corr[7];
    for (int ical=0; ical<7;ical++ ){
        char tit[80];
        sprintf(tit,"calomax_%d-baseline_%d",ical+1,ical+1);
        h[ical]= new TH1F(tit,tit,500,0,2000);
    }


    // Loop on entries of the tree
    for (int i=0; i<t->GetEntries();i++) {
        t->GetEntry(i);

        calo[0]=max[3]-baseline[3];
        for (int ical=1;ical<7;ical++) calo[ical]=max[7+ical]-baseline[7+ical];

        calsum=0;
        calsumcorr=0;
        for (int ical=0;ical<7;ical++){
            h[ical]->Fill(calo[ical]);
            //h_corr[ical]->Fill((int)(calo[ical]*scaling[ical]));
            calsum+=calo[ical];
            calsumcorr+=calo[ical]*scaling[ical];
        }
        h_sum->Fill(calsum);
        h_sum_corr->Fill(calsumcorr);
        //Timing
        h_time->Fill((256-falling_edge[2])*10);
        if(calsumcorr>6840) h_time_cut->Fill((256-falling_edge[2])*10);
        h_time_ene->Fill((256-falling_edge[2])*10,calsumcorr);
    }

    TCanvas *c4 = new TCanvas("c4","c4",1000,700);
    c4->Divide(4,2);

    for (int ical=0;ical<7;ical++) {
        c4->cd(ical+1);
        h[ical]->Draw();
    }
    c4->cd(8)->SetLogy(); h_sum->Draw();
    h_sum_corr->Draw("same");

    //Timing

    TCanvas* c5=new TCanvas("c5","c5",1000,700);
    c5->Divide(2,2);
    c5->cd(1);
    h_time->Draw();
    h_time->GetXaxis()->SetTitle("time [ns]");
    h_time->GetYaxis()->SetTitle("counts");

    c5->cd(2);
    h_time_cut->Draw();

    c5->cd(3);
    h_time_ene->Draw("colz");
    c5->cd(4);
    h_sum_corr->Draw();





}
