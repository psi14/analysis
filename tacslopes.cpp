#include "TTree.h"
#include "TGraph.h"
#include "TF1.h"
#include "TH1F.h"
#include <stdio.h>


void tacslopes(TTree * tree, int startrun, int stoprun){

  char cut[1024];
  
  TF1 * line = new TF1("line","pol1",0,1000);
  TH1F * slope = new TH1F("slope","slope",stoprun-startrun +1,startrun-0.5,stoprun + 0.5);
  TH1F * offset = new TH1F("offset","offset",stoprun-startrun +1,startrun-0.5,stoprun + 0.5); 
  
  int bin = 0;
  
  for(int run = startrun; run <= stoprun; run++){
    sprintf(cut, "max[0] > 10000 && max[0] < 15000 && (256-falling_edge[2])*10 < 1000 && (256-falling_edge[2])*10 > 0 && run == %i", run);
    int nev = tree->Draw("max[0]:(256-falling_edge[2])*10",cut
    ,"goff");
    
    bin++;
    
    if(nev < 100)
      continue;
    
    //printf("%i\n",nev);
  
    TGraph *g = new TGraph(nev,tree->GetV2(),tree->GetV1());
    //g->Draw("AP");
    
    g->Fit(line,"QN","",0,1000);
    
    printf("Run: %i Events: %i Offset: %f Slope: %f\n", run, nev, line->GetParameter(0), line->GetParameter(1));
    slope->SetBinContent(bin,line->GetParameter(1));
    slope->SetBinError(bin,line->GetParError(1));
    
    offset->SetBinContent(bin,line->GetParameter(0));
    offset->SetBinError(bin,line->GetParError(0));
       
  }
  
  slope->Draw();

}