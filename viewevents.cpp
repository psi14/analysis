/**
 * PSI14 - viewevents.cpp
 *
 * version of daqana for event-by-event display
 *
 * @author Carsten Grzesik <grzesik@physi.uni-heidelberg.de>
 * @author Sebastian Schenk <Sebastian.Schenk@stud.uni-heidelberg.de>
 * @author Dominick Cichon <cichon@stud.uni-heidelberg.de>
 */
 
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include <TApplication.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TGraph.h>
#include <TTree.h>

#include "helper_functions.hpp"

int viewevents(char *rootfname, unsigned int NSAMPLES)
{   
    // see if file fname exists
    if (access(rootfname, F_OK) == -1) {
        printf("File %s does not exist! \n", rootfname);
        return -1;
    } else {
        printf("Reading %s \n", rootfname);
    }
    
    // load the TTree from the file
    TFile *rootfile = new TFile(rootfname, "READ");
    TTree *rawtree = (TTree *)rootfile->Get("raw");
    printf("Total entries: %llu \n", rawtree->GetEntries());

    // define global constants for baseline fits and number of channels
    const unsigned int CALO_POL_DEG = 3;
    const unsigned int MISC_POL_DEG = 1;
    const unsigned int NCHANNELS = 14;

    // define raw tree variables and branch strings
    uint32_t ts1;
    uint32_t ts2;
    uint16_t channels[NCHANNELS][NSAMPLES];
    uint16_t smoothchannels[NCHANNELS][NSAMPLES];
    
    char branchname[256];

    // define branch variables
    unsigned int run;
    // double falling_edge[14];
    // double rising_edge[14];
    double max[14];
    double min[14];
    // double baseline[14];
    // double baseline2[14];
    double integral[14];
    double pospeaks[14];
    double negpeaks[14];
    double pospeakpos1[14];
    double pospeakpos2[14];
    double pospeakheight1[14];
    double pospeakheight2[14];
    double negpeakpos1[14];
    double negpeakpos2[14];
    double negpeakheight1[14];
    double negpeakheight2[14];
    double chisquare[14];
    unsigned int ndf[14];

    // define variables used by data processing algorithms
    unsigned int nmin, nmax;
    unsigned int currpos;
    unsigned int peakvals[3];
    double fitparams[20];
    double fitparams_smooth[20];

    // create TCanvas and TGraphs for printing events and various variables
    TCanvas *paintbrd = new TCanvas("paintbrd", "paintboard", 800, 600);
    TGraph *waveform = new TGraph(NSAMPLES);
    TGraph *smoothwave = new TGraph(NSAMPLES);
    TF1 *fitfunc = new TF1("fitfunc", "pol3", 0, 1024);
    TF1 *fitfunc2 = new TF1("fitfunc2", "pol1", 0, 1024);
    unsigned int eventcounter = 0;
    double baselinecontrib;

    // set drawing options
    waveform->SetTitle("waveform");
    waveform->SetLineColor(kBlue);
    smoothwave->SetTitle("waveform");
    smoothwave->SetLineColor(kRed);
    fitfunc->SetLineColor(kGreen);
    fitfunc->SetLineWidth(2);
    fitfunc2->SetLineColor(kGreen);
    fitfunc2->SetLineWidth(2);

    // set raw tree branch addresses
    for(unsigned int channel = 0; channel < NCHANNELS; channel++){
        sprintf(branchname, "c%02i", channel);
        rawtree->SetBranchAddress(branchname, channels[channel]);
    }
    rawtree->SetBranchAddress("run", &run);
    rawtree->SetBranchAddress("ts1", &ts1);
    rawtree->SetBranchAddress("ts2", &ts2);

    // define other daqana variables
    uint64_t nevents = rawtree->GetEntries();
    double dummy_double;
    unsigned int dummy_int;
        
    for(uint64_t event = 0; event < nevents; event++){
        rawtree->GetEntry(event);
        ++eventcounter;
        for(unsigned int channel = 0; channel < NCHANNELS; channel++) {
            // calculate event parameters

             /* calculations of old parameters
              *
              * baseline[channel] = _baseline(20 , channels[channel]);
              *
              * baseline2[channel] = _baselineCalo(20 , channels[channel],500);
              *
              * falling_edge[channel] = _find_falling_edge(NSAMPLES, baseline[channel],
              *                                            0.5, channels[channel]);
              * rising_edge[channel] = _find_rising_edge(NSAMPLES, baseline[channel],
              *                                          0.5, channels[channel]);
              */

            nmin = _find_min(NSAMPLES, channels[channel]);
            nmax = _find_max(NSAMPLES, channels[channel]);
            min[channel] = channels[channel][nmin];
            max[channel] = channels[channel][nmax];

            // use different baseline estimation and integration
            // intervals for calo channels
            if (channel == 3 || channel > 7) {
                // use all samples outside of peak area for fitting
                _bl(NSAMPLES, 260, 49, 211, 140, 624, CALO_POL_DEG,
                    &(chisquare[channel]), &(ndf[channel]), fitparams,
                    channels[channel]);
                integral[channel] = _integral_bl(180, 450, CALO_POL_DEG,
                                                 fitparams,
                                                 channels[channel]);
                baselinecontrib = _get_bl(180, 450, CALO_POL_DEG,
                                          fitparams, channels[channel]);
            } else {
                // use first and last 50 samples for fitting
                _bl(NSAMPLES, 512, 463, 50, 462, 50, MISC_POL_DEG,
                    &(chisquare[channel]), &(ndf[channel]), fitparams,
                    channels[channel]);
                integral[channel] = _integral_bl(0, 1023, MISC_POL_DEG,
                                                 fitparams,
                                                 channels[channel]);
                baselinecontrib = _get_bl(0, 1023, CALO_POL_DEG,
                                          fitparams, channels[channel]);
            }
	     
            //peak related parameter determination begins here
            currpos = 0;
            pospeaks[channel] = 0;
            negpeaks[channel] = 0;
            pospeakpos1[channel] = 0;
            pospeakpos2[channel] = 0;
            pospeakheight1[channel] = 0;
            pospeakheight2[channel] = 0;
            negpeakpos1[channel] = 0;
            negpeakpos2[channel] = 0;
            negpeakheight1[channel] = 0;
            negpeakheight2[channel] = 0;
             
            // only use smoothing for calo channels
            if (channel==3 || channel > 7)
            {
                _smooth_wave(NSAMPLES, 5, channels[channel],
                             smoothchannels[channel]);
                _bl(NSAMPLES, 260, 49, 211, 140, 624, CALO_POL_DEG,
                    &(dummy_double), &(dummy_int), fitparams_smooth,
                    smoothchannels[channel]);

                while (_find_peak(NSAMPLES, CALO_POL_DEG, fitparams_smooth,
                                  50, peakvals, currpos,
                                  smoothchannels[channel])==0) {
                    ++(pospeaks[channel]);
                    currpos = peakvals[2];

                    switch ((int) pospeaks[channel]) {
                        case 1:
                            pospeakpos1[channel] = peakvals[0];
                            pospeakheight1[channel] = channels[channel][peakvals[0]];
                            break;
                        case 2:
                            pospeakpos2[channel] = peakvals[0];
                            pospeakheight2[channel] = channels[channel][peakvals[0]];
                            break;
                    }
                }
                currpos = 0;
                while (_find_peak(NSAMPLES, CALO_POL_DEG, fitparams_smooth,
                                  -200, peakvals, currpos,
                                  smoothchannels[channel])==0) {
                    ++(negpeaks[channel]);
                    currpos = peakvals[2];
                
                    switch ((int) negpeaks[channel]) {
                        case 1:
                            negpeakpos1[channel] = peakvals[0];
                            negpeakheight1[channel] = channels[channel][peakvals[0]];
                            break;
                        case 2:
                            negpeakpos2[channel] = peakvals[0];
                            negpeakheight2[channel] = channels[channel][peakvals[0]];
                            break;
                    }
                }
            } else {
                while (_find_peak(NSAMPLES, MISC_POL_DEG, fitparams, 400,
                                  peakvals, currpos, channels[channel])==0) {
                    ++(pospeaks[channel]);
                    currpos = peakvals[2];

                    switch ((int) pospeaks[channel]) {
                        case 1:
                            pospeakpos1[channel] = peakvals[0];
                            pospeakheight1[channel] = channels[channel][peakvals[0]];
                            break;
                        case 2:
                            pospeakpos2[channel] = peakvals[0];
                            pospeakheight2[channel] = channels[channel][peakvals[0]];
                            break;
                    }
                }
                currpos = 0;
                while (_find_peak(NSAMPLES, MISC_POL_DEG, fitparams, -400,
                                  peakvals, currpos, channels[channel])==0) {
                    ++(negpeaks[channel]);
                    currpos = peakvals[2];

                    switch ((int) negpeaks[channel]) {
                        case 1:
                            negpeakpos1[channel] = peakvals[0];
                            negpeakheight1[channel] = channels[channel][peakvals[0]];
                            break;
                        case 2:
                            negpeakpos2[channel] = peakvals[0];
                            negpeakheight2[channel] = channels[channel][peakvals[0]];
                            break;
                    }
                }
            }
             
            // fill TGraphs and print them
            for (unsigned int i = 0; i < NSAMPLES; ++i) {
                waveform->SetPoint(i, i, channels[channel][i]);

                if (channel == 3 || channel > 7)
                    smoothwave->SetPoint(i, i, smoothchannels[channel][i]);
            }

            if (channel == 3 || channel > 7) {
                fitfunc->SetParameters(fitparams);
                paintbrd->Clear();
                smoothwave->Draw();
                smoothwave->GetXaxis()->SetTitle("time [ADC units]");
                smoothwave->GetYaxis()->SetTitle("voltage [ADC units]");
                smoothwave->GetYaxis()->SetTitleOffset(1.5);
                waveform->Draw("SAME");
                fitfunc->Draw("SAME");
                paintbrd->Update();
            } else {
                fitfunc2->SetParameters(fitparams);
                paintbrd->Clear();
                waveform->Draw();
                waveform->GetXaxis()->SetTitle("time [ADC units]");
                waveform->GetYaxis()->SetTitle("voltage [ADC units]");
                waveform->GetYaxis()->SetTitleOffset(1.5);
                fitfunc2->Draw("SAME");
                paintbrd->Update();
            }

            printf("-------------------------------\n");
            printf("Viewing channel/event: %2u/%4u\n", channel, eventcounter);
            printf("-------------------------------\n");
            printf("Fit polynomial offset: %f\n", fitparams[0]);
            printf("Integral: %f\n", integral[channel]);
            printf("Baseline contribution: %f\n", baselinecontrib);
            printf("Chisquare/NDF: %f\n", chisquare[channel]/
                                          (ndf[channel]>0?ndf[channel]:1));
            printf("Max: %f\n", max[channel]);
            printf("Min: %f\n", min[channel]);
            printf("Positive peaks: %f\n", pospeaks[channel]);
            printf("Negative peaks: %f\n\n", negpeaks[channel]);
            getchar();
        } 
    }

    rootfile->Close();

    return 0;
}


int main(int argc, char **argv)
{
    TApplication rootstuff("rootstuff", &argc, argv);
  
    uint16_t nsamples = strtol(argv[1], NULL, 10);
    char rootfname[256];
    
    FILE *pathp;
    
    char fpath[256];
    pathp = fopen("datapath.txt", "r");
    if (!pathp) {
        printf("Unable to read path to data file \n");
        return -1;
    }
    fgets(fpath, sizeof(fpath), pathp);
    fclose(pathp);
    
    
    if(argc == 3){
        sprintf(rootfname, "%s/ROOT/run_%s.root", strtok(fpath, "\n"), argv[2]);
        viewevents(rootfname, nsamples);
    } else if (argc == 4) {
        uint32_t runstart = strtol(argv[2], NULL, 10);
        uint32_t runstop = strtol(argv[3], NULL, 10);
        if (runstop < runstart) {
            uint32_t tmp = runstop;
            runstop = runstart;
            runstart = tmp;
        }
        for (unsigned int run = runstart; run <= runstop; run++) {
            sprintf(rootfname, "%s/ROOT/run_%u.root", strtok(fpath, "\n"), run);
            viewevents(rootfname, nsamples);
        }
    } else {
        return -1;
    }
    
    return 0;
}

