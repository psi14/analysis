/**
 * PSI14 - daqread.C
 *
 * read binary files from the output of the two logic box DAQ
 * and write them to a corresponding ROOT file in a TTree
 *
 * @author Carsten Grzesik <grzesik@physi.uni-heidelberg.de>
 * @author Sebastian Schenk <Sebastian.Schenk@stud.uni-heidelberg.de>
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>

#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TApplication.h>


// get the size of a file in bytes
long getfilesize(FILE *file)
{
    long curpos, endpos;
    curpos = ftell(file);
    fseek(file, 0, 2);
    endpos = ftell(file);
    fseek(file, curpos, 0);
    
    return endpos;
}

int waveview(uint32_t runnumber, uint32_t channelmask)
{
    /**
     * read the header file of our binaries given in the consecutive form
     * run number           32 bit
     * UTC timestamp        64 bit
     * event sample size     8 bit
     * clock                 8 bit
     * free spacing         16 bit
     * 
     * TOTAL size of header: 16 bytes
     */
    FILE *filep;
    FILE *pathp;
    
    char fpath[256];
    char fname[256];
    pathp = fopen("datapath.txt", "r");
    if (!pathp) {
        printf("Unable to read path to data file \n");
        return -1;
    }
    fgets(fpath, sizeof(fpath), pathp);
    sprintf(fname, "%srun_%u.bin", strtok(fpath, "\n"), runnumber);
    fclose(pathp);
    
    // open file
    filep = fopen(fname, "rb");
    if (!filep) {
        printf("Unable to open file %s \n", fname);
        return -1;
    }
    
    // get filesize in bytes
    long size = getfilesize(filep);
    
    // read header
    uint32_t buffer[6];
    if (!fread(buffer, sizeof(uint32_t), 4, filep)) {
        printf("Unable to read file %s \n", fname);
        return -1;
    } else {
        printf("Reading %s \n", fname);
    }
    
    uint32_t run = buffer[0];
    uint64_t UTC_timestamp = *((uint64_t *)(&(buffer[1])));
    uint8_t eventsamplesize = buffer[3];
    uint8_t clock = buffer[3] >> 8;
    
    /** change encoded numbers to useful information
     *
     * encoding clock:
     * 0      100 MHz
     * 1       50 MHz
     * 2       33 MHz
     * 3       25 MHz
     *
     * encoding event sample size:
     * 0           16
     * 1           32
     * 2           64
     * 3          128
     * 4          256
     * 5          512
     * 6         1024
     * 7         2048
     */
    uint16_t heventsamplesize = pow(2, 4 + eventsamplesize);
    uint16_t hclock;
    switch (clock) {
        case 0:
            hclock = 100;
            break;
        case 1:
            hclock = 50;
            break;
        case 2:
            hclock = 33;
            break;
        case 3:
            hclock = 25;
            break;
    }
    printf("size: %li bytes \n", size);
    printf("run number: %u \n", run);
    printf("UTC timestamp: %lu s \n", UTC_timestamp);
    printf("event sample size: %u \n", heventsamplesize);
    printf("clock: %u MHz \n", hclock);
    
    
   
    
    
  
    const unsigned int NCHANNELS = 14;
    
    uint16_t channeldata[NCHANNELS][heventsamplesize];
    int32_t cdataint[NCHANNELS][heventsamplesize];
    uint32_t ts1, ts2;
    uint32_t eventnr1, eventnr2;
    uint32_t channels, samples;

    
    uint32_t mismatchedevents = 0;
    
    TH2F * back = new TH2F("Wave","Wave",10,-0.5,1023.5,10,0,17000);
    TCanvas *c1 = new TCanvas("wave","wave",1024,600);
    
    int32_t xvalues[heventsamplesize];
    for(unsigned int s = 0; s < heventsamplesize; s++){
        xvalues[s] = s;
    }
    
    
    while(!feof(filep)){
        fread(buffer, sizeof(uint32_t), 6, filep);       
        // set ts, eventnr, channels, samples
        eventnr1 = buffer[0];
        ts1 = buffer[1];
        eventnr2 = buffer[2];
        ts2 = buffer[3];
        
        fread(&(channeldata[0][0]), sizeof(uint16_t), 
                NCHANNELS*heventsamplesize, filep);
	
        
        for(unsigned int c = 0; c < NCHANNELS; c++){
            for(unsigned int s = 0; s < heventsamplesize; s++){
                cdataint[c][s] = channeldata[c][s];
            }
        }
        
         // check consistency
        // *WARNING* events with mismatching eventnr are ignored
        if (eventnr1 != eventnr2) {
            mismatchedevents++;
            cout << eventnr1 << " " << eventnr2 << endl;
        } else
            continue;
        
        back->Draw();
        
        TGraph * graphs[NCHANNELS];
        
        for(unsigned int c = 0; c < NCHANNELS; c++){
           graphs[c] = new TGraph(heventsamplesize, xvalues, cdataint[c]);
           graphs[c]->SetLineColor(c+1);
           if(channelmask & (1<<c)){
            //cout << (channelmask & (1<<c)) << " -  " << channelmask << " " <<(1<<c) << endl;
            graphs[c]->Draw("SAME");
           }
        }
        
        c1->Update();
        
	   
	    
	    char xx = getchar();
        
        if(xx=='q')
           break;
        
        for(unsigned int c = 0; c < NCHANNELS; c++){
            delete graphs[c];
        }
                
    } 
 
    
    // close files
    fclose(filep);
    printf("Number of ignored events (mismatching eventnumber): %u \n", mismatchedevents);
    
    
   
    
    return 0; 
}
